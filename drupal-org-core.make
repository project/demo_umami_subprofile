api = 2
core = 8.x
projects[drupal][type] = core
projects[drupal][version] = 8.5.x
projects[drupal][patch][] = https://www.drupal.org/files/issues/drupal-n1356276-417-d8.5.%2A.patch
